﻿using System;

namespace Fractions
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction x = new Fraction(7);
            Fraction a = new Fraction(4, -8);
            Fraction b = new Fraction(2, 5);
            Fraction z = new Fraction(81, 17, 21);
            Fraction c; // переменная, в которую будет записываться результат 

            Console.WriteLine("Представление в виде обыкновенной дроби:" + a.ToString() + " " + b.ToString());

            Console.Write("Введите знак операции: ");
            string sign = Console.ReadLine();

            Fraction d = new Fraction(4, 7);
            Console.WriteLine($"Числитель: {d[0]}");
            Console.WriteLine($"Знаменатель: {d[1]}");
            Console.WriteLine("Знак дроби: " + d.GetSign());

            a.ChangeNumerator += ChangeNum;
            a.ChangeDenumerator += ChangeDenum;

            b.ChangeNumerator += ChangeNum;
            b.ChangeDenumerator += ChangeDenum;

            a.Numinator_1 = -2;
            a.Denominator_1 = 5;

            b.Numinator_2 = -4;
            b.Denominator_2 = 5;

            switch (sign)
            {
                case "+":
                    c = a + b;
                    Console.WriteLine("Проверка на сложение: " + a.ToString() + " + " + b.ToString() + " = " + c.ToString());
                    Console.WriteLine("Десятичная дробь: " + c.ToDouble());
                    break;
                case "-":
                    c = a - b;
                    Console.WriteLine("Проверка на вычитание: " + a.ToString() + " - " + b.ToString() + " = " + c.ToString());
                    Console.WriteLine("Десятичная дробь: " + c.ToDouble());
                    break;
                case "*":
                    c = a * b;
                    Console.WriteLine("Проверка на умножение: " + a.ToString() + " * " + b.ToString() + " = " + c.ToString());
                    Console.WriteLine("Десятичная дробь: " + c.ToDouble());
                    break;
                case "/":
                    c = a / b;
                    Console.WriteLine("Проверка на деление: " + a.ToString() + "/" + b.ToString() + "=" + c.ToString());
                    if (b.Denominator_2 == 0) 
                        Console.WriteLine("Ошибка! Деление на ноль");
                    Console.WriteLine("Десятичная дробь: " + c.ToDouble());
                    break;
                default:
                    Console.WriteLine("Неизвестная операция");
                    break;
            }
            Console.ReadKey();
        }
        public static void ChangeNum(Fraction fraction, int num)
        {
            Console.WriteLine("Числитель изменился");
        }
        
        public static void ChangeDenum(Fraction fraction, int denum)
        {
            Console.WriteLine("Знаменатель изменился");
        }
    }
}
